﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public enum GameObjectGroups
    {
        player,
        enemy,
        playerShot,
        enemyShot,
        collectable
    }
    public class GameObjectRegistry : IEnumerable
    {
        LinkedList<GameObject> registry;
        LinkedList<Alive> aliveRegistry;
        LinkedList<Drawable> drawableRegistry;

        Dictionary<GameObjectGroups, LinkedList<GameObject>> objectGroups = new Dictionary<GameObjectGroups, LinkedList<GameObject>>();

        public GameObjectRegistry()
        {
            registry = new LinkedList<GameObject>();
            aliveRegistry = new LinkedList<Alive>();
            drawableRegistry = new LinkedList<Drawable>();
        }

        public void Add(GameObject item)
        {
            registry.AddLast(item);
            if (item is Alive)
            {
                aliveRegistry.AddLast((Alive)item);
            }
            if (item is Drawable)
            {
                drawableRegistry.AddLast((Drawable)item);
            }
            if (item.Group.HasValue)
            {
                if (!objectGroups.ContainsKey(item.Group.Value)) objectGroups.Add(item.Group.Value, new LinkedList<GameObject>());

                objectGroups[item.Group.Value].AddLast(item);
            }
        }

        public void Remove(GameObject item)
        {
            registry.Remove(item);
            if (item is Alive)
            {
                aliveRegistry.Remove((Alive)item);
            }
            if (item is Drawable)
            {
                drawableRegistry.Remove((Drawable)item);
            }
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)registry).GetEnumerator();
        }

        public IEnumerable<GameObject> GetEnumerable(GameObjectGroups group)
        {
            if (!objectGroups.ContainsKey(group)) return null;
            return objectGroups[group];
        }

        public IEnumerable GetAliveEnumerator()
        {
            return (IEnumerable)aliveRegistry;
        }


        public IEnumerable GetDrawableEnumerator()
        {
            return (IEnumerable)drawableRegistry;
        }

    }
}
