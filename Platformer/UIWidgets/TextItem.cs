﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    class TextItem: UIWidget
    {

        protected string Text = null;


        public TextItem(Vector2 position, common.Level level, string text) : base(position, level)
        {
            Text = text;
        }

        public override void Load()
        {
        }

        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {

            if (!(Text is null))
                spriteBatch.DrawString(level.userInterface.font, Text, position, Color.White);
        }


    }
}
