﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    static class MapLoader
    {
        public static string[,] ParseMap(string map)
        {
            
            string[] lines = map.Split(Environment.NewLine);
            int width = lines[0].Split(',').Length;
            string[,] items = new string[lines.Length, width];
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                string[] lineItems = line.Split(',');
                for (int j = 0; j < width; j++)
                {
                    items[i, j] = lineItems[j];
                }
            }
            return items;
        }

        public static Vector2 GetPlayerStartPosition(string[,] map)
        {
            int height = map.GetUpperBound(0);
            int width = map.GetUpperBound(1);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (map[i, j] == "p") return new Vector2(GameMap.TILE_SIZE * j, GameMap.TILE_SIZE * i);
                }
            }
            return Vector2.Zero;
        }

        public static GameObject[] LoadObjects(string[,] map, Level level)
        {
            List<GameObject> objects = new List<GameObject>();
            int height = map.GetUpperBound(0);
            int width = map.GetUpperBound(1);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (map[i, j] == "k")
                    {
                        GameObject obj = new GameObjects.Knight(new Vector2(GameMap.TILE_SIZE * j, GameMap.TILE_SIZE * i), level);
                        objects.Add(obj);
                    }
                    else if (map[i,j] == "y")
                    {
                        GameObject obj = new GameObjects.Book(new Vector2(GameMap.TILE_SIZE * j, GameMap.TILE_SIZE * i), level);
                        objects.Add(obj);
                    } else if (map[i,j] == "g")
                    {
                        GameObject obj = new GameObjects.Stairs(new Vector2(GameMap.TILE_SIZE * j, GameMap.TILE_SIZE * i), level);
                        objects.Add(obj);
                    }
                    else if (map[i, j] == "c")
                    {
                        GameObject obj = new GameObjects.Cafedra(new Vector2(GameMap.TILE_SIZE * j, GameMap.TILE_SIZE * i), level);
                        objects.Add(obj);
                    }
                }
            }

            return objects.ToArray();
        }
    }
}
