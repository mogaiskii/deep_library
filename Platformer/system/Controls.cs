﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.system
{
    public class Controls
    {
        public KeyboardState Keyboard
        {
            get; private set;
        }
        public GamePadState GamePad
        {
            get; private set;
        }
        public float Left
        {
            get
            {
                if (Keyboard.IsKeyDown(Keys.A) || Keyboard.IsKeyDown(Keys.Left))
                {
                    return 1;
                }
                if (GamePad.ThumbSticks.Left.X < 0)
                {
                    return -1 * GamePad.ThumbSticks.Left.X;
                }
                return 0;
            }
        }

        public bool LeftClick
        {
            get
            {
                if (Mouse.GetState().LeftButton == ButtonState.Pressed) return true;
                return false;
            }
        }

        public bool RightClick
        {
            get
            {
                if (Mouse.GetState().RightButton == ButtonState.Pressed) return true;
                return false;
            }
        }

        public float Right
        {
            get
            {
                if (Keyboard.IsKeyDown(Keys.D) || Keyboard.IsKeyDown(Keys.Right))
                {
                    return 1;
                }
                if (GamePad.ThumbSticks.Left.X > 0)
                {
                    return GamePad.ThumbSticks.Left.X;
                }
                return 0;
            }
        }
        public float Up
        {
            get
            {
                if (Keyboard.IsKeyDown(Keys.W) || Keyboard.IsKeyDown(Keys.Up))
                {
                    return 1;
                }
                if (GamePad.ThumbSticks.Left.Y < 0)
                {
                    return -1 * GamePad.ThumbSticks.Left.Y;
                }
                return 0;
            }
        }
        public float Down
        {
            get
            {
                if (Keyboard.IsKeyDown(Keys.S) || Keyboard.IsKeyDown(Keys.Down))
                {
                    return 1;
                }
                if (GamePad.ThumbSticks.Left.Y > 0)
                {
                    return GamePad.ThumbSticks.Left.Y;
                }
                return 0;
            }
        }
        public bool Exit
        {
            get
            {
                return Keyboard.IsKeyDown(Keys.Escape) || GamePad.Buttons.Back == ButtonState.Pressed;
            }
        }

        public Vector2 MousePosition  // at game, not counting camera
        {
            get
            {
                return Mouse.GetState().Position.ToVector2() * (1/Game1.SCALE);
            }
        }

        public void Update(KeyboardState keyboardState, GamePadState gamePadState)
        {
            Keyboard = keyboardState;
            GamePad = gamePadState;
        }
    }
}
