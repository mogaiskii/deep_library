﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Platformer.common;

namespace Platformer
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private common.Level currentLevel;
        private Level nextLevel = null;
        private common.LevelLoader.LevelLoader levelLoader = new common.LevelLoader.StoryLoader();
        public system.Controls controls { get; private set; } = new system.Controls();

        public void NextLevel(string name)
        {
            nextLevel = levelLoader.LoadLevel(name, this, Content);
            nextLevel.Load();
        }

        public static int TARGET_SIZE = 64;
        public static float SCALE = (float)TARGET_SIZE / common.GameMap.TILE_SIZE / 3 * 4;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {

            base.Initialize();
            _graphics.IsFullScreen = false;
            _graphics.PreferredBackBufferWidth = 1000;  // set this value to the desired width of your window
            _graphics.PreferredBackBufferHeight = 734;   // set this value to the desired height of your window
            _graphics.ApplyChanges();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            currentLevel = levelLoader.LoadLevel("main", this, Content);
            currentLevel.Load();
        }

        protected override void Update(GameTime gameTime)
        {
            changeLevel = false;
            controls.Update(Keyboard.GetState(), GamePad.GetState(0));
            //if (controls.Exit)
            //{
            //    system.ConsoleDebug.PrintConfig();
            //    Exit();
            //}
            //system.ConsoleDebug.Update(this);

            try
            {
                currentLevel.Update(gameTime);
            }
            catch (ChangeLevelExc)
            {
                Console.WriteLine("next level");
                currentLevel = nextLevel;
                nextLevel = null;
                changeLevel = true;
            }
            base.Update(gameTime);
        }
        bool changeLevel = false;
        protected override void Draw(GameTime gameTime)
        {
            if (changeLevel) return;
            double elapsedTime = gameTime.ElapsedGameTime.TotalMilliseconds;

            GraphicsDevice.Clear(Color.Black);
            currentLevel.Draw(_spriteBatch, elapsedTime);

            base.Draw(gameTime);
        }

        public void GameOver()
        {
            currentLevel = levelLoader.LoadLevel("main", this, Content);
            // TODO: экран смери
            currentLevel.Load();
        }
    }
}
