﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level7 : TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,b,b,b,b,b,b,b,bp
bw,e,e,e,e,e,e,bp,e,e,e,be
bw,e,y,e,bp,k,e,bp,e,p,e,be
bp,b,b,e,bp,g,e,bp,e,b,e,be
bw,e,e,e,bp,bp,b,bp,e,e,e,be
bw,e,e,e,e,b,k,b,e,k,e,be
bw,e,e,b,e,e,e,e,e,b,e,be
bw,e,y,e,e,e,bp,e,e,e,e,be
bp,bs,bs,bs,bs,bs,bp,bs,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 7;
        }
        public override string NextLevel()
        {
            return "level8";
        }
    }
}
