﻿using Microsoft.Xna.Framework;
using Platformer.common;
using Platformer.GameObjects;
using Platformer.system;
using Platformer.Weapons;
using System;

namespace Platformer.Engines
{
    public class PlayerEngine : Engine
    {
        public static float speed = 1.2f;
        Controls controls;

        Weapon leftWeapon;
        Weapon rightWeapon;
        Weapon defaultWeapon;

        public PlayerEngine(Level level, Controls controls): base(level)
        {
            this.controls = controls;
            defaultWeapon = new Mop();
            leftWeapon = new AttackBook();
        }

        public Weapon LeftWeapon()
        {
            if (leftWeapon is null) return defaultWeapon;
            return leftWeapon;
        }

        public Weapon RightWeapon()
        {
            if (rightWeapon is null) return defaultWeapon;
            return rightWeapon;
        }

        public new void Update(double elapsedTime)
        {
            Vector2 movement = GetInputMovement();
            if (movement.X != 0 || movement.Y != 0)
            {
                movement.Normalize();
                movement = movement * (float)speed * (float)elapsedTime;
                Vector2 nextCoords = GetCollidedNextCoords(movement, Player.bounds);

                gameObject.MoveTo(nextCoords);
            }

            if (controls.LeftClick || controls.RightClick)
            {
                Attack(controls.LeftClick, controls.RightClick);
            }

            LeftWeapon().UpdateCooldown(elapsedTime);
            RightWeapon().UpdateCooldown(elapsedTime);
        }

        private void Attack(bool leftClick, bool rightClick)
        {
            Vector2 target = controls.MousePosition;

            GameObject shot = null;
            if (leftClick)
            {
                if (!(leftWeapon is null))
                {
                    shot = leftWeapon.Perform(rightWeapon is null, gameObject, gameObject.coordinates, target, level);
                }
                else shot = defaultWeapon.Perform(leftWeapon is null, gameObject, gameObject.coordinates, target, level);
            }

            if (rightClick)
            {
                if (!(rightWeapon is null))
                {
                    shot = rightWeapon.Perform(leftWeapon is null, gameObject, gameObject.coordinates, target, level);
                }
                else shot = defaultWeapon.Perform(leftWeapon is null, gameObject, gameObject.coordinates, target, level);
            }

            if (!(shot is null))
            {
                level.AddObjects(shot);
            }
        }

        Vector2 GetInputMovement()
        {
            Vector2 movement = new Vector2();
            if (controls.Left > 0)
            {
                movement.X = -1;
            }
            if (controls.Right > 0)
            {
                movement.X = 1;
            }
            if (controls.Down > 0)
            {
                movement.Y = 1;
            }
            if (controls.Up > 0)
            {
                movement.Y = -1;
            }
            return movement;
        }

    } 
}
