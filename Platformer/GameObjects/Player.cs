﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using Platformer.Engines;
using Platformer.Weapons.Shots;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.GameObjects
{
    public class Player : GameObject, Drawable, Alive
    {
        public static int PlayerStartHealth = 200;
        public override int StartHealth { get => PlayerStartHealth; set => PlayerStartHealth = value; }

        PlayerEngine engine;
        public static Rectangle bounds = new Rectangle(150, 150, 230, 330);
        public override Rectangle Bounds { get => Player.bounds; protected set => Player.bounds = value; }

        public override GameObjectGroups? Group { get => GameObjectGroups.player; protected set => base.Group = value; }

        public Player(String textureName, Vector2 coordinates, Level level, PlayerEngine playerEngine) : base(textureName, coordinates, level) {
            engine = playerEngine;
            engine.SetOwner(this);
        }

        public Weapons.Weapon LeftWeapon()
        {
            return engine.LeftWeapon();
        }

        public Weapons.Weapon RightWeapon()
        {
            return engine.RightWeapon();
        }

        bool staying = true;
        int cFrameTimer = 0;
        int orientation = 0;
        int frame = 0;
        int frameTimer = 150;
        int maxFrame = 3;
        Rectangle SourceRect()
        {
            int line = orientation;
            if (!staying)
            {
                if (cFrameTimer < 0)
                {
                    cFrameTimer = frameTimer;
                    frame += 1;
                    if (frame > maxFrame) frame = 1;
                }
            } else
            {
                frame = 0;
            }
            return new Rectangle(frame * GameMap.TILE_SIZE, line * GameMap.TILE_SIZE, GameMap.TILE_SIZE, GameMap.TILE_SIZE);
        }

        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;
            spriteBatch.Draw(texture2D, coordinates, SourceRect(), _textureBgColor);
            cFrameTimer -= (int)elapsedTime;
        }

        public override void OnMovement(Vector2 diffMovement)
        {
            staying = false;
            if (diffMovement.X > 0) orientation = 3;
            else if (diffMovement.X < 0) orientation = 1;
            else if (diffMovement.Y > 0) orientation = 0;
            else orientation = 2;
        }
        public new void Update(double elapsedTime)
        {
            staying = true;
            if (currentDamageCoolDown > 0) currentDamageCoolDown -= (int)elapsedTime;
            engine.Update(elapsedTime);

            IEnumerable<GameObject> shots = level.GetOverlaps(GetSelfZone(), GameObjectGroups.enemyShot);
            if (!(shots is null))
            {
                foreach (GameObject shot in shots)
                {
                    ((WeaponShot)shot).PerformDamage(this);
                }
            }

            IEnumerable<GameObject> collects = level.GetOverlaps(GetSelfZone(), GameObjectGroups.collectable);
            if (!(collects is null))
            {
                foreach (GameObject collect in collects)
                {
                    ((Collectable)collect).PerformCollect(this);
                }
            }
        }
    }
}
