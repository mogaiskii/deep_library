﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    class PauseButton: Button
    {
        public PauseButton(Vector2 position, common.Level level, system.Controls controls) : base(position, level, controls)
        {
            textureName = "pause";
        }

        protected override void OnClick()
        {
            base.OnClick();
            level.Pause();
        }
    }
}
