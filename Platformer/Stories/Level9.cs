﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level9 : TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,bp,b,b,b,b,b,b,bp
bw,e,e,e,bp,e,e,e,e,e,e,be
bw,e,g,e,b,e,e,e,b,e,y,be
bw,e,e,y,e,e,e,e,e,e,b,bp
bp,b,b,e,e,e,k,b,e,k,e,be
bw,e,e,e,k,b,e,e,e,bp,e,be
bw,e,b,e,e,e,e,bp,e,b,e,be
bw,e,e,e,e,e,e,bp,e,p,e,be
bp,bs,bs,bs,bs,bs,bs,bs,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 9;
        }
        public override string NextLevel()
        {
            return "final";
        }
    }
}
