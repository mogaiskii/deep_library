﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public class ResourceLoader
    {
        ContentManager manager;

        Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

        Dictionary<string, SpriteFont> fonts = new Dictionary<string, SpriteFont>();

        public ResourceLoader(ContentManager manager)
        {
            this.manager = manager;
        }

        Texture2D Load(string textureName)
        {
            Texture2D texture = manager.Load<Texture2D>(textureName);
            textures.Add(textureName, texture);
            return texture;
        }
        SpriteFont LoadFont(string fontName)
        {
            SpriteFont font = manager.Load<SpriteFont>(fontName);
            fonts.Add(fontName, font);
            return font;
        }
        public void PreloadTextures(params string[] textureNames)
        {
            foreach (string name in textureNames)
            {
                Load(name);
            }
        }
        public void PreloadFonts(params string[] fontNames)
        {
            foreach (string name in fontNames)
            {
                LoadFont(name);
            }
        }

        public Texture2D GetTexture(string textureName)
        {
            if (textures.ContainsKey(textureName)) return textures[textureName];
            return Load(textureName);
        }

        public SpriteFont GetFont(string fontName)
        {
            if (fonts.ContainsKey(fontName)) return fonts[fontName];
            return LoadFont(fontName);
        }
    }
}
