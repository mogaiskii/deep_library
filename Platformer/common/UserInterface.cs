﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer.common
{
    public class UserInterface : Drawable, Alive
    {
        public List<UIWidget> widgets { get; protected set; } = new List<UIWidget>();
        public SpriteFont font { get; protected set; }
        Level level;
        bool looplock = false;
        public UserInterface(Level level)
        {
            this.level = level;
        }

        public void AddWidget(UIWidget widget)
        {
            if (looplock) AddLater(widget);
            else widgets.Add(widget);
        }
        List<UIWidget> toAdd = new List<UIWidget>();
        private void AddLater(UIWidget widget)
        {
            toAdd.Add(widget);
        }

        public void RemoveWidget(UIWidget widget)
        {
            if (looplock) RemoveLater(widget);
            else widgets.Remove(widget);
        }
        List<UIWidget> toRemove = new List<UIWidget>();
        private void RemoveLater(UIWidget widget)
        {
            toRemove.Add(widget);
        }

        public void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            foreach (UIWidget widget in widgets)
            {
                widget.Draw(spriteBatch, elapsedTime);
            }

            foreach (var item in toAdd)
            {
                widgets.Add(item);
            }
            toAdd.Clear();

            foreach (var item in toRemove)
            {
                widgets.Remove(item);
            }
            toRemove.Clear();

        }

        public void Update(double elapsedTime)
        {
            looplock = true;
            foreach (UIWidget widget in widgets)
            {
                if (widget is Alive)
                {
                    ((Alive)widget).Update(elapsedTime);
                }
            }
            looplock = false;
        }

        public void Load()
        {
            foreach (UIWidget widget in widgets)
            {
                widget.Load();
            }
            font = level.loader.GetFont("File");
        }
    }
}
