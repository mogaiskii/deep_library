﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using Platformer.GameObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Weapons.Shots
{
    class WeaponShot : GameObject, Alive, Drawable
    {
        int damage;
        Vector2 target;
        protected float speed = 3f;
        protected int timeToLive = 100; // ms
        protected virtual int TimeToLive { get => timeToLive; set => timeToLive = value; }
        protected Vector2 movement;

        protected int additionalTTL = 300;
        protected virtual int AdditionalTTL { get => additionalTTL; set => additionalTTL = value; }

        protected bool deactivated = false;

        private GameObjectGroups? group = null;
        public override GameObjectGroups? Group { get => group; protected set => base.Group = value; }


        private Rectangle bounds = new Rectangle(50, 50, 480, 480);
        public override Rectangle Bounds { get => bounds; protected set => bounds = value; }

        public WeaponShot(Vector2 coordinates, Level level, int damage, Vector2 target, GameObject invoker): base (coordinates, level)
        {
            this.damage = damage;
            this.target = target - new Vector2(Bounds.X + Bounds.Width / 2, Bounds.Y + Bounds.Height / 2);
            movement = this.target - coordinates;
            movement.Normalize();
            if (invoker.Group.HasValue && invoker.Group.Value == GameObjectGroups.player) group = GameObjectGroups.playerShot;
            else group = GameObjectGroups.enemyShot;
        }

        internal void PerformDamage(GameObject target)
        {
            if (deactivated) return;
            TimeToLive = 0;
            Random r = new Random();
            int realDamage = (int)((float)damage * (float)r.Next(80, 110) / 100f);
            target.HandleDamage(realDamage);
        }

        public override void Update(double elapsedTime)
        {
            TimeToLive -= (int)elapsedTime;
            if (TimeToLive < 0)
            {
                if (deactivated == false)
                {
                    deactivated = true;
                    speed /= 2f;
                    TimeToLive += AdditionalTTL;
                } else
                {
                    level.RemoveObject(this);
                }
            }

            Vector2 cMovement = movement * (float)speed * (float)elapsedTime;
            
            Vector2 nextCoords = coordinates + cMovement;

            MoveTo(nextCoords);
        }

    }
}
