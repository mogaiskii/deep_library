﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Platformer.common;
using Platformer.GameObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Main: Story
    {
        public override Level LoadLevel(Game1 game, ContentManager content)
        {
            Level level = new Level(content, game);

            level.loader.PreloadTextures("pause", "button", "mainmenu");
            level.loader.PreloadFonts("File");
            foreach (UIWidget item in GetWidgets(level, game.controls))
            {
                level.userInterface.AddWidget(item);
            }
            return level;
        }

        List<UIWidget> GetWidgets(Level level, system.Controls controls)
        {
            List<UIWidget> widgts = new List<UIWidget>();
            widgts.Add(new UIWidgets.SimpleImage(new Vector2(0, 0), level, level.loader.GetTexture("mainmenu"), 4.2f));

            widgts.Add(new UIWidgets.ExitButton(new Vector2(4000, 2900), level, controls));
            widgts.Add(new UIWidgets.StartButton(new Vector2(4000, 2300), level, controls));

            //widgts.Add(new UIWidgets.LevelAnimation(new Vector2(-1000, 0), level, 0));

            return widgts;
        }

    }
}
