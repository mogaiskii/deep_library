﻿using Microsoft.Xna.Framework;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.GameObjects
{
    public class Collectable: GameObject
    {
        public Collectable(Vector2 coordinates, Level level) : base(coordinates, level)
        {
        }

        protected virtual void OnCollect(Player player) { }

        public void PerformCollect(Player player)
        {
            OnCollect(player);
        }
    }
}
