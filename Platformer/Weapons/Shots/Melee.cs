﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using System;

namespace Platformer.Weapons.Shots
{
    class Melee: WeaponShot
    {
        private Rectangle bounds = new Rectangle(12, 12, 500, 500);
        private Vector2 origin = new Vector2(290, 290);
        private Rectangle source = new Rectangle(0, 0, 512, 512);
        private float drawAngle = 0f;
        public override Rectangle Bounds { get => bounds; protected set => bounds = value; }
        public Melee(Vector2 coordinates, Level level, int damage, Vector2 target, GameObject invoker) : base(coordinates, level, damage, target, invoker)
        {
            textureName = "melee";
            //drawAngle = AngleBetween(movement, normalAngleVector);
            drawAngle = (float)Math.PI*4/3 + VectorToAngle(movement);
            speed = 1.5f;
            timeToLive = 70;
            AdditionalTTL = 100;
            this.coordinates += new Vector2(invoker.Bounds.X + invoker.Bounds.Width / 2f, invoker.Bounds.Y + invoker.Bounds.Height / 2f) + movement * 100;
        }

        float VectorToAngle(Vector2 vector)
        {
            return (float)Math.Atan2(vector.Y, vector.X);
        }


        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;
            spriteBatch.Draw(texture2D, coordinates, source, _textureBgColor, drawAngle, origin, 1f, SpriteEffects.None, 1);
        }
    }
}
