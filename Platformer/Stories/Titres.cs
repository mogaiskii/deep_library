﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Titres : Story
    {
        public override Level LoadLevel(Game1 game, ContentManager content)
        {
            Level level = new Level(content, game);

            level.loader.PreloadTextures("pause", "button");
            level.loader.PreloadFonts("File");
            foreach (UIWidget item in GetWidgets(level, game.controls))
            {
                level.userInterface.AddWidget(item);
            }
            return level;
        }

        List<UIWidget> GetWidgets(Level level, system.Controls controls)
        {
            List<UIWidget> widgts = new List<UIWidget>();

            widgts.Add(new UIWidgets.ExitButton(new Vector2(2000, 2500), level, controls));
            widgts.Add(new UIWidgets.TextItem(new Vector2(2000, 2000), level, "The end"));

            return widgts;
        }
    }
}
