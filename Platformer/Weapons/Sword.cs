﻿using Microsoft.Xna.Framework;
using Platformer.common;


namespace Platformer.Weapons
{
    class Sword : Weapon
    {
        public Sword()
        {
            damage = 20;
            singleWeaponDamage = 20;
        }

        public override GameObject Perform(bool singleWeapon, GameObject invoker, Vector2 invokePoint, Vector2 targetPoint, Level level)
        {
            if (currentCooldown > 0) return null;
            currentCooldown = cooldown;
            int realDamage;
            if (singleWeapon) realDamage = singleWeaponDamage;
            else realDamage = damage;
            return new Shots.Melee(invokePoint, level, realDamage, targetPoint, invoker);
        }
    }
}
