﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    public class ExitButton : Button
    {
        public ExitButton(Vector2 position, common.Level level, system.Controls controls) : base(position, level, controls)
        {
            textureName = "button";
            Text = "Exit";
        }

        protected override void OnClick()
        {
            level.Exit();
        }
    }
}
