﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common.LevelLoader
{
    public class StoryLoader : LevelLoader
    {
        Dictionary<String, Story> Stories = new Dictionary<String, Story> {
            {"main", new Stories.Main()},
            {"start", new Stories.Start()},
            {"level2", new Stories.Level2() },
            {"level3", new Stories.Level3() },
            {"level4", new Stories.Level4() },
            {"level5", new Stories.Level5() },
            {"level6", new Stories.Level6() },
            {"level7", new Stories.Level7() },
            {"level8", new Stories.Level8() },
            {"level9", new Stories.Level9() },
            {"final", new Stories.GameFinal() },
            {"titres", new Stories.Titres() },
        };

        public Level LoadLevel(string name, Game1 game, ContentManager content)
        {
            Story story;
            if (!Stories.TryGetValue(name, out story))
            {
                throw new LevelNotFoundException();
            }

            return story.LoadLevel(game, content);
        }
    }
}
