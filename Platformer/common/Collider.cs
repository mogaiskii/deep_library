﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public class Collider
    {
        Level level;
        GameMap map;
        public Collider(Level level, GameMap map)
        {
            this.level = level;
            this.map = map;
        }

        public bool HasCollision(int x, int y)
        {
            if (map.GetCollision(x, y) == TileCollision.blocking) return true;
            return false;
        }

        public bool HasCollision(Rectangle rectangle)
        {
            return HasCollision(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2);
        }
    }
}
