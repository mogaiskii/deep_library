﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level5 : TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,b,b,b,b,b,b,b,bp
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,b,y,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,b,e,e,e,be
bw,e,e,e,e,e,e,e,e,y,e,be
bw,e,k,e,b,bp,k,e,bp,g,e,be
bw,e,b,e,e,be,e,e,bp,e,e,be
bw,e,p,e,e,e,e,e,bp,e,e,be
bp,bs,bs,bs,bs,bs,bs,bs,bp,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 5;
        }
        public override string NextLevel()
        {
            return "level6";
        }
    }
}
