﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level8 :TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,bp,b,b,b,b,b,b,bp
bw,e,e,k,b,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,e,b,y,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,e,k,bp,e,y,b,e,e,be
bp,b,b,e,b,bp,e,b,e,e,e,be
bw,e,g,y,e,bp,bp,e,e,b,p,be
bw,e,e,e,e,e,bp,e,e,e,e,be
bp,bs,bs,bs,bs,bs,bp,bs,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 8;
        }
        public override string NextLevel()
        {
            return "level9";
        }
    }
}
