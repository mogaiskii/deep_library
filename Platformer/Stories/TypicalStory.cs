﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Platformer.common;
using Platformer.GameObjects;

namespace Platformer.Stories
{
    public class TypicalStory: Story
    {
        public virtual string GetMap()
        {
            string mapStr = @"bp,b,b,b,bp,b,b,b,b,b,b,bp
bw,e,e,e,bp,e,e,e,e,e,e,be
bw,e,g,e,bp,e,e,e,e,e,e,be
bw,e,e,e,bp,e,e,e,e,e,e,be
bw,e,e,e,bp,e,e,bs,e,p,e,be
bw,e,e,e,b,k,e,bp,e,e,e,be
bw,e,e,e,e,e,e,bp,e,e,e,be
bw,e,e,e,e,e,e,bp,e,e,e,be
bp,bs,bs,bs,bs,bs,bs,bp,bs,bs,bs,bp";
            return mapStr;
        }
        public virtual string NextLevel()
        {
            return "main";
        }
        public virtual UIWidgets.LevelAnimation GetAnimation(int num, Level level)
        {
            if (num < 0)
                return null;
            return new UIWidgets.LevelAnimation(Vector2.Zero, level, num);
        }

        public override Level LoadLevel(Game1 game, ContentManager content)
        {
            Level level = new Level(content, game);
            GameMap map = new GameMap(level);

            string[,] mapItems = MapLoader.ParseMap(GetMap());
            Vector2 playerPos = MapLoader.GetPlayerStartPosition(mapItems);
            Player player = GetPlayer(level, game.controls, playerPos);
            GameObject[] tiles = map.LoadTiles(mapItems);
            GameObject[] objects = MapLoader.LoadObjects(mapItems, level);
            level.SetPlayer(player);
            level.AddObjects(tiles);
            level.AddObjects(player);
            level.AddObjects(objects);
            level.nextLevel = NextLevel();
            level.loader.PreloadTextures(
                "fire", "knight", "platform", "player", "stairs", "melee", "book", "icon", "icon cool down", "attack", "armor", "pause", "button", "war", "ice", "cafedra", "wall_left", "wall_right", "wall_bottom", "wall_top", "wood"
                );
            level.loader.PreloadFonts("File");
            UIWidgets.LevelAnimation anim = GetAnimation(GetNum(), level);
            if (!(anim is null))
            {
                level.animation = anim;
                anim.Load();
            }
            foreach (UIWidget item in GetWidgets(level, game.controls))
            {
                level.userInterface.AddWidget(item);
            }
            return level;
        }

        protected virtual int GetNum()
        {
            return -1;
        }

        List<UIWidget> GetWidgets(Level level, system.Controls controls)
        {
            List<UIWidget> widgts = new List<UIWidget>();

            widgts.Add(new UIWidgets.HealthBar(new Vector2(3000, 20), level, (Player)level.player));
            widgts.Add(new UIWidgets.WeaponBar(new Vector2(500, 20), level, (Player)level.player));
            widgts.Add(new UIWidgets.PauseButton(new Vector2(4500, 20), level, controls));

            return widgts;
        }

        Player GetPlayer(Level level, system.Controls controls, Vector2 coordinates)
        {
            Engines.PlayerEngine engine = new Engines.PlayerEngine(level, controls);
            return new Player("players", coordinates, level, engine);
        }

    }
}
