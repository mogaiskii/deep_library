﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Platformer.common
{
    public class Engine : Alive
    {
        public static Rectangle bounds;
        protected GameObject gameObject;
        protected Level level;

        public Engine(Level level)
        {
            this.level = level;
        }
        public void SetOwner(GameObject owner)
        {
            gameObject = owner;
        }

        public void Update(double elapsedTime)
        {
        }

        protected bool RayHasNoObstacles(Point from, Point target)
        {
            //return true;
            Vector2 distance = (target - from).ToVector2();
            int steps = (int)Math.Floor(distance.Length() / (float)GameMap.TILE_SIZE);
            distance.Normalize();
            steps *= 2;
            distance /= 2;
            for (int i = 0; i < steps; i++)
            {
                Vector2 checkPoint = from.ToVector2() + distance * i * (float)GameMap.TILE_SIZE;
                bool collision = level.collider.HasCollision((int)Math.Floor(checkPoint.X), (int)Math.Floor(checkPoint.Y));
                if (collision) return false;
            }

            return true;
        }

        protected Vector2 GetCollidedNextCoords(Vector2 movement, Rectangle bounds)
        {
            int movementLength = (int)Math.Abs(Math.Round(movement.Length()));
            bool horBlock = false;
            bool vertBlock = false;

            Vector2 nextCoords = gameObject.coordinates + movement;
            if (movement.X != 0)
            {
                float nextX = nextCoords.X;
                if (movement.X < 0)
                {
                    nextX += bounds.X;
                }
                else if (movement.X > 0)
                {
                    nextX += bounds.X + bounds.Width;
                }
                //Проверяем оба баунда, и верхний угол , и нижний. Может быть например зацеп только верхним пикселем или только нижним при движении вбок
                //a Floor для того,чтобы не давать в минус уходить
                if (
                    level.collider.HasCollision((int)Math.Floor(nextX), (int)Math.Floor(gameObject.coordinates.Y + bounds.Y)) ||
                    level.collider.HasCollision((int)Math.Floor(nextX), (int)Math.Floor(gameObject.coordinates.Y + bounds.Y + bounds.Height))
                    )
                {
                    nextCoords.X -= movement.X;
                    horBlock = true;
                }
            }
            if (movement.Y != 0)
            {
                float nextY = nextCoords.Y;
                if (movement.Y < 0)
                {
                    nextY += bounds.Y;
                }
                else if (movement.Y > 0)
                {
                    nextY += bounds.Y + bounds.Height;
                }
                if (
                    level.collider.HasCollision((int)Math.Floor(gameObject.coordinates.X + bounds.X), (int)Math.Floor(nextY)) ||
                    level.collider.HasCollision((int)Math.Floor(gameObject.coordinates.X + bounds.X + bounds.Width), (int)Math.Floor(nextY))
                    )
                {
                    nextCoords.Y -= movement.Y;
                    vertBlock = true;
                }
            }

            if (horBlock && !vertBlock)
            {
                nextCoords.Y -= movement.Y;
                nextCoords.Y += movementLength * Math.Sign(movement.Y);
            }
            if (vertBlock && !horBlock)
            {
                nextCoords.X -= movement.X;
                nextCoords.X += movementLength * Math.Sign(movement.X);
            }

            return nextCoords;

        }
    }
}
