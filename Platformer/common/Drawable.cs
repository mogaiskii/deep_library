﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public interface Drawable
    {
        public void Draw(SpriteBatch spriteBatch, double elapsedTime);
    }
}
