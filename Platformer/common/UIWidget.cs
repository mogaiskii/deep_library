﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer.common
{
    public class UIWidget : Drawable
    {
        public Vector2 position;
        protected Level level;
        public UIWidget(Vector2 position, Level level)
        {
            this.position = position;
            this.level = level;
        }

        public virtual void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
        }

        public virtual void Load()
        {
        }
    }
}
