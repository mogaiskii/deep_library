﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Platformer.common;
using Platformer.GameObjects;

namespace Platformer.Stories
{
    class Level2: TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,bp,b,b,b,b,b,b,bp
bw,e,e,e,bp,e,e,e,e,e,e,be
bw,e,g,e,bp,e,e,e,e,e,e,be
bw,e,e,e,bp,e,e,e,e,e,e,be
bw,e,e,e,bp,e,e,bs,e,p,e,be
bw,e,e,e,b,k,e,bp,e,e,e,be
bw,e,e,e,e,e,e,bp,e,e,e,be
bw,e,e,e,e,e,e,bp,e,e,e,be
bp,bs,bs,bs,bs,bs,bs,bp,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 2;
        }
        public override string NextLevel()
        {
            return "level3";
        }

    }
}
