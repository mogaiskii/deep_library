﻿using Microsoft.Xna.Framework;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.GameObjects
{
    public class Cafedra: Collectable, Drawable
    {
        public static Rectangle bounds = new Rectangle(150, 60, 230, 420);
        public override Rectangle Bounds { get => Knight.bounds; protected set => Knight.bounds = value; }
        public override GameObjectGroups? Group { get => GameObjectGroups.collectable; protected set => base.Group = value; }

        public Cafedra(Vector2 coordinates, Level level) : base(coordinates, level)
        {
            bounds = new Rectangle(0, 0, GameMap.TILE_SIZE, GameMap.TILE_SIZE);
            textureName = "cafedra";
        }

        protected override void OnCollect(Player player)
        {
            base.OnCollect(player);
            level.NextLevel(level.nextLevel);
        }
    }
}
