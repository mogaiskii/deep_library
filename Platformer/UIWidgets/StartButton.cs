﻿using Microsoft.Xna.Framework;
using Platformer.common;
using Platformer.system;

namespace Platformer.UIWidgets
{
    internal class StartButton : Button
    {
        public StartButton(Vector2 position, Level level, Controls controls) : base(position, level, controls)
        {
            textureName = "button";
            Text = "Start";
        }

        protected override void OnClick()
        {
            base.OnClick();
            level.NextLevel("start");
        }
    }
}