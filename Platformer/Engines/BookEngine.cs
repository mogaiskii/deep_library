﻿using Platformer.common;
using Platformer.GameObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Engines
{
    class BookEngine : EnemyEngine
    {
        public BookEngine(Level level, GameObject owner) : base(level, owner)
        {
            fireDistance = 3000;
        }

        protected override void Attack()
        {
            if (((Book)gameObject).laying) return;
            base.Attack();
        }
    }
}
