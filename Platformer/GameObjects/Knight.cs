﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using Platformer.Weapons;
using System;

namespace Platformer.GameObjects
{
    class Knight: Enemy
    {
        public static Rectangle bounds = new Rectangle(150, 60, 230, 420);
        public override Rectangle Bounds { get => Knight.bounds; protected set => Knight.bounds = value; }

        public static int KnightHealth = 100;
        public override int StartHealth { get => KnightHealth; set => KnightHealth = value; }

        public static Weapon KnightWeapon = new Sword();
        public override Weapon UsedWeapon { get => KnightWeapon; protected set => KnightWeapon = value; }


        //описывать данные для загрузки класса в сам класс. в т ч текстуру и прочее. а вот загружать текстуру лучше уровнем выше, чтобы загрузить её лишь раз!
        public Knight(Vector2 coordinates, common.Level level) : base(coordinates, level)
        {
            bounds = new Rectangle(150, 60, 230, 420);
            textureName = "war";
        }

        bool staying = true;
        int cFrameTimer = 0;
        int orientation = 0;
        int frame = 0;
        int frameTimer = 150;
        int maxFrame = 3;
        Rectangle SourceRect()
        {
            int line = orientation;
            if (!staying)
            {
                if (cFrameTimer < 0)
                {
                    cFrameTimer = frameTimer;
                    frame += 1;
                    if (frame > maxFrame) frame = 1;
                }
            }
            else
            {
                frame = 0;
            }
            return new Rectangle(frame * GameMap.TILE_SIZE, line * GameMap.TILE_SIZE, GameMap.TILE_SIZE, GameMap.TILE_SIZE);
        }

        public override void OnMovement(Vector2 diffMovement)
        {
            staying = false;
            if (diffMovement.X > 0) orientation = 3;
            else if (diffMovement.X < 0) orientation = 1;
            else if (diffMovement.Y > 0) orientation = 0;
            else orientation = 2;
        }
        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;
            spriteBatch.Draw(texture2D, coordinates, SourceRect(), _textureBgColor);
            cFrameTimer -= (int)elapsedTime;
        }

    }
}
