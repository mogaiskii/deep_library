﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    class SimpleImage: UIWidget
    {
        protected Texture2D image = null;
        protected float scale = 1f;

        public SimpleImage(Vector2 position, common.Level level, Texture2D image, float scale) : base(position, level)
        {
            this.image = image;
            this.scale = scale;
        }

        public override void Load()
        {
        }

        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {

            if (!(image is null))
            {
                //spriteBatch.Draw(image, position, Color.White);
                spriteBatch.Draw(image, position, image.Bounds, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0f);
            }
        }
    }
}
