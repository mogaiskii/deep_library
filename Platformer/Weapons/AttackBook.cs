﻿using Microsoft.Xna.Framework;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Weapons
{
    class AttackBook: Weapon
    {
        public AttackBook()
        {
            damage = 20;
            singleWeaponDamage = 37;
        }

        public override string IconName { get; protected set; } = "attack";

        public override GameObject Perform(bool singleWeapon, GameObject invoker, Vector2 invokePoint, Vector2 targetPoint, Level level)
        {
            if (currentCooldown > 0) return null;
            currentCooldown = cooldown;
            int realDamage;
            if (singleWeapon) realDamage = singleWeaponDamage;
            else realDamage = damage;
            return new Shots.Fire(invokePoint, level, realDamage, targetPoint, invoker);
        }
    }
}
