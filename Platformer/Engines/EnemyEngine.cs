﻿using Microsoft.Xna.Framework;
using Platformer.common;
using Platformer.GameObjects;
using Platformer.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Engines
{
    class EnemyEngine : common.Engine
    {
        public static float speed = 1.4f;

        bool knowDeWay = false;
        Vector2 lastSeen;
        const int SEEN_TIMER_COOLDOWN = 700; // 0.7sec
        int seenTimer = 0;
        List<Point> path;
        bool hasNextPoint = false;
        Vector2 nextPoint;

        public int fireDistance = 500;
        public virtual int FireDistance { get => fireDistance; set => fireDistance = value; }

        public Weapon weapon = null;
        public virtual Weapon UsedWeapon { get => weapon; protected set => weapon = value; }

        public EnemyEngine(Level level, GameObject owner) : base(level)
        {
            this.SetOwner(owner);
            if (!(owner.UsedWeapon is null))
            {
                weapon = owner.UsedWeapon;
                fireDistance = weapon.FireDistance;
            }
        }

        public new void Update(double elapsedTime)
        {
            Vector2 movement = GetVectorMovement();
            if (movement.LengthSquared() > 300)
            {
                movement.Normalize();
                movement = movement * (float)speed * (float)elapsedTime;
                Rectangle bounds = ((Enemy)gameObject).Bounds;
                Vector2 nextCoords = GetCollidedNextCoords(movement, bounds);

                gameObject.MoveTo(nextCoords);
            }
            if (seenTimer > 0)
            {
                seenTimer -= (int)elapsedTime;
            }

            float distance = DistanceToPlayer();
            if (distance < FireDistance)
            {
                Attack();
            }

            UsedWeapon.UpdateCooldown(elapsedTime);

        }

        protected virtual void Attack()
        {
            if (UsedWeapon is null) return;
            if (!PlayerIsVisible()) return;
            GameObject shot = UsedWeapon.Perform(true, gameObject, gameObject.coordinates, level.player.coordinates, level);
            if (!(shot is null))
            {
                level.AddObjects(shot);
            }
        }

        protected float DistanceToPlayer()
        {
            return (float)Math.Sqrt((gameObject.coordinates - level.player.coordinates).LengthSquared());
        }

        Vector2 GetVectorMovement()
        {

            float distanceToPlayer = DistanceToPlayer();
            if (distanceToPlayer < GameMap.TILE_SIZE * 1.2)
            {
                knowDeWay = false;
                hasNextPoint = false;
                seenTimer = 0;
                if (distanceToPlayer < fireDistance)
                {
                    //Comment that if calibrate bounds would be required
                    return Vector2.Zero;
                }
                return level.player.coordinates - gameObject.coordinates;
            }

            WatchPlayer();

            if (knowDeWay)
            {
                if (!hasNextPoint)
                {
                    nextPoint = path[0].ToVector2();
                    path.RemoveAt(0);
                    if (path.Count == 0) knowDeWay = false;
                    hasNextPoint = true;
                }
            }

            if (hasNextPoint)
            {
                float dist = (gameObject.coordinates.X - nextPoint.X) * (gameObject.coordinates.X - nextPoint.X) + (gameObject.coordinates.Y - nextPoint.Y) * (gameObject.coordinates.Y - nextPoint.Y);
                dist = (float)Math.Sqrt(dist);
                if (dist < bounds.Width)
                {
                    hasNextPoint = false;
                } else
                {
                    return nextPoint - gameObject.coordinates;
                }
            }

            

            return Vector2.Zero;
        }

        bool ShouldUpdatePath()
        {
            float distance = (float)Math.Sqrt((lastSeen - level.player.coordinates).LengthSquared());
            if (distance > GameMap.TILE_SIZE || seenTimer == 0) return true;
            return false;
        }

        public bool PlayerIsVisible()
        {
            Vector2 watchPoint = new Vector2(gameObject.coordinates.X + ((Enemy)gameObject).Bounds.X, gameObject.coordinates.Y + ((Enemy)gameObject).Bounds.Y);
            Vector2 targetPoint1 = new Vector2(level.player.coordinates.X + Player.bounds.X + level.player.Bounds.Width / 2, level.player.coordinates.Y + level.player.Bounds.Y);
            Vector2 targetPoint2 = new Vector2(level.player.coordinates.X + Player.bounds.X + level.player.Bounds.Width / 2, level.player.coordinates.Y + level.player.Bounds.Y + level.player.Bounds.Height);
            return RayHasNoObstacles(watchPoint.ToPoint(), targetPoint1.ToPoint()) || RayHasNoObstacles(watchPoint.ToPoint(), targetPoint2.ToPoint());
        }

        void WatchPlayer()
        {if (ShouldUpdatePath())
            {
                if (PlayerIsVisible())
                {
                    lastSeen = new Vector2(level.player.coordinates.X, level.player.coordinates.Y);
                    knowDeWay = true;
                    hasNextPoint = false;
                    path = Pathfinder.FindPath(gameObject.coordinates.ToPoint(), lastSeen.ToPoint(), level);
                    if (path is null) knowDeWay = false;
                    seenTimer = SEEN_TIMER_COOLDOWN;
                }
            }
        }
    }
}
