﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Weapons
{
    public class Weapon
    {
        public static int cooldown { get; private set; } = 1000; // ms
        public int currentCooldown { get; protected set; } = 0;

        public static int defaultFireDistance = 500;
        public int FireDistance { get => defaultFireDistance; protected set => defaultFireDistance = value; }

        protected int damage = 10;
        protected int singleWeaponDamage = 18;

        protected int shieldEfficency = 0;
        protected int singleWeaponShieldEfficency = 0;

        public virtual string IconName { get; protected set; } = "mop";

        public virtual GameObject Perform(bool singleWeapon, GameObject invoker, Vector2 invokePoint, Vector2 targetPoint, Level level)
        {
            return null;
        }

        public int CooldownPercent()
        {
            return (int)Math.Ceiling((float)currentCooldown / (float)cooldown * 100f);
        }

        public void UpdateCooldown(double elapsedTime)
        {
            if (currentCooldown > 0) currentCooldown -= (int)elapsedTime;
            if (currentCooldown < 0) currentCooldown = 0;
        }
    }
}
