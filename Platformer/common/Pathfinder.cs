﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Platformer.common
{
    static class Pathfinder
    {
        class PathPoint : IEquatable<PathPoint>
        {
            public int x;
            public int y;
            public int cost = int.MaxValue;
            public PathPoint prev;
            public PathPoint(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
            public PathPoint(int x, int y, int cost)
            {
                this.x = x;
                this.y = y;
                this.cost = cost;
            }
            public PathPoint(int x, int y, PathPoint prev)
            {
                this.x = x;
                this.y = y;
                this.prev = prev;
            }
            public PathPoint(int x, int y, PathPoint prev, int cost)
            {
                this.x = x;
                this.y = y;
                this.prev = prev;
                this.cost = cost;
            }

            //Returns real path, not indexed on map
            internal List<Point> GetPath()
            {
                PathPoint current = this;
                List<Point> path = new List<Point>();
                while (!(current is null))
                {
                    path.Add(new Point(current.x * GameMap.TILE_SIZE, current.y * GameMap.TILE_SIZE));
                    current = current.prev;
                }
                path.Reverse();
                return path;
            }
            public override int GetHashCode()
            {
                return HashCode.Combine(x, y);
            }

            public bool Equals([AllowNull] PathPoint other)
            {
                if (other is null) return false;
                return this.x == other.x && this.y == other.y;
            }
        }

        static PathPoint PeekPoint(List<PathPoint> reachable, Point goal)
        {
            int minCost = int.MaxValue;
            PathPoint best = null;

            foreach (var node in reachable)
            {
                int costStartToNode = node.cost;
                int costNodeToGoal = GetDistance(node, goal);
                int totalCost = costNodeToGoal + costStartToNode;

                if (minCost > totalCost)
                {
                    best = node;
                    minCost = totalCost;
                }
            }

            reachable.Remove(best);
            return best;
        }

        private static int GetDistance(PathPoint node, Point goal)
        {
            return (int)Math.Round(Math.Sqrt((node.x - goal.X) * (node.x - goal.X) + (node.y - goal.Y) * (node.y - goal.Y)));
        }

        public static List<Point> FindPath(Point from, Point to, Level level)
        {
            Point mapFrom = new Point(from.X / GameMap.TILE_SIZE, from.Y / GameMap.TILE_SIZE);
            Point mapTo = new Point(to.X / GameMap.TILE_SIZE, to.Y / GameMap.TILE_SIZE);

            List<PathPoint> reachable = new List<PathPoint> { new PathPoint(mapFrom.X, mapFrom.Y, 0) };
            HashSet<PathPoint> explored = new HashSet<PathPoint>();

            while (reachable.Count > 0)
            {
                PathPoint node = PeekPoint(reachable, to);
                if (node.x == mapTo.X && node.y == mapTo.Y)
                {
                    return node.GetPath();
                }
                explored.Add(node);

                HashSet<Point> adjPoints = GetAdjacentPoints(new Point(node.x, node.y), level);
                foreach (Point item in adjPoints)
                {
                    PathPoint newPoint = new PathPoint(item.X, item.Y);
                    if (explored.Contains(newPoint)) continue;

                    if (!reachable.Contains(newPoint)) reachable.Add(newPoint);
                    else
                    {
                        int origIndex = reachable.IndexOf(newPoint);
                        PathPoint origPoint = reachable[origIndex];
                        if (node.cost + 1 < origPoint.cost)
                        {
                            origPoint.prev = node;
                            origPoint.cost = node.cost + 1;
                        }
                    }

                }

            }

            return null;
        }

        private static HashSet<Point> GetAdjacentPoints(Point point, Level level)
        {
            return level.map.GetAdjacentPoints(point.X, point.Y);
        }
    }
}
