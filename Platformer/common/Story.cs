﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public abstract class Story
    {
        public abstract Level LoadLevel(Game1 game, ContentManager content);
    }
}
