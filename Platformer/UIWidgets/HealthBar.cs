﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    class HealthBar: common.UIWidget
    {
        public static int offset = common.GameMap.TILE_SIZE;
        GameObjects.Player player;
        protected Texture2D texture2D;
        static string textureName = "heart";
        public static int hearts = 3;
        static int heartMagnitude = 20;
        int heartPosition = 0;
        int heartVector = 7;

        public HealthBar(Vector2 position, common.Level level, GameObjects.Player player): base(position, level)
        {
            this.player = player;
        }

        // TODO: выгрузка из памяти
        public override void Load()
        {
            texture2D = level.loader.GetTexture(textureName);
        }

        private float YHeartShake(double elapsedTime)
        {
            heartPosition += heartVector;
            if (Math.Abs(heartPosition) > heartMagnitude)
            {
                heartPosition = Math.Sign(heartPosition) * heartMagnitude;
                heartVector *= -1;
            }
            return heartPosition;
        }
        int HeartCount()
        {
            float step = (int)Math.Ceiling(player.StartHealth / (float)hearts);
            int count = (int)Math.Ceiling(player.currentHealth / step);
            return count;
        }

        bool HeartShaking()
        {
            float step = (int)Math.Ceiling(player.StartHealth / (float)hearts);
            int count = (int)Math.Ceiling(player.currentHealth / step);
            float idealHealth = (int)Math.Ceiling(player.StartHealth / (float)hearts) * hearts;
            float cHealth = player.currentHealth + (idealHealth - GameObjects.Player.PlayerStartHealth);
            if (step * count > cHealth) return true;
            return false;
        }

        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;
            int count = HeartCount();
            for (int i = 0; i < count; i++)
            {
                Vector2 offsetVector = new Vector2((hearts - i - 1) * offset, 0);
                if (i == count - 1 && HeartShaking())
                {
                    offsetVector.Y = YHeartShake(elapsedTime);
                }
                spriteBatch.Draw(texture2D, position + offsetVector, Color.White);
            }
        }

    }
}
