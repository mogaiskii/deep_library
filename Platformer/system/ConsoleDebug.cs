﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Platformer.system
{
    static class ConsoleDebug
    {
        static Dictionary<string, string> config = new Dictionary<string, string>();
        public static void Update(Game1 game)
        {
            if (Console.KeyAvailable)
            {
                Console.ReadKey(true);
                Console.WriteLine("Enter command: ");
                string command = Console.ReadLine();
                HandleCommand(command, game);
            }
        }

        static void HandleCommand(string command, Game1 game)
        {
            string[] tokens = command.Split(' ');
            if (tokens.Length == 0) return;

            if (tokens[0] == "set")
            {
                HandleSet(tokens);
                return;
            }
            if (tokens[0] == "restart")
            {
                game.GameOver();
            }
        }

        static void HandleSet(string[] tokens)
        {
            try
            {
                string command = tokens[0];
                string variable = tokens[1];
                string value = tokens[3];
                SetVariable(variable, value);
            }
            catch (Exception)
            {
                Console.WriteLine("Bad input. Syntax: `set variable = value`. Spaces required.");
            }
        }

        static void SetVariable(string variable, string value)
        {
            if (variable == "speed") {
                Engines.PlayerEngine.speed = float.Parse(value, CultureInfo.InvariantCulture);
            }
            else if (variable == "bounds")
            {
                string[] bounds = value.Split('/');
                int x = int.Parse(bounds[0]);
                int y = int.Parse(bounds[1]);
                int width = int.Parse(bounds[2]);
                int height = int.Parse(bounds[3]);
                GameObjects.Player.bounds = new Microsoft.Xna.Framework.Rectangle(x, y, width, height);
            }
            else if (variable == "timesize")
            {
                int size = int.Parse(value);
                common.GameMap.TILE_SIZE = size;
            }
            else
            {
                //has no such command
                return;
            }

            if (!config.ContainsKey(variable))
            {
                config.Add(variable, value);
            }
            else
            {
                config[variable] = value;
            }
        }

        public static void PrintConfig()
        {
            Console.Clear();
            foreach (var item in config.Keys)
            {
                Console.WriteLine($"{item} : {config[item]}");
            }
        }
    }
}
