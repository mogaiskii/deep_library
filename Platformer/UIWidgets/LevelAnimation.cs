﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    public class LevelAnimation: UIWidget
    {
        Texture2D texture2D;
        float scale = 4.2f;
        float pminiscale = 0.5f;
        string textureName = "towermap";
        Texture2D playerTexture2D;
        string playerTextureName = "playermini";
        Vector2 offsetVector = Vector2.Zero;
        int n_l = 0;
        float targetPos;
        float speed = .2f;
        float cd = 500;
        public bool stoped { get; protected set; } = false;
        public LevelAnimation(Vector2 position, common.Level level, int levelNum) : base(position, level)
        {
            offsetVector = new Vector2(5300, NextLevel(levelNum));
            targetPos = NextLevel(levelNum + 1);
            n_l = levelNum;
        }
        public override void Load()
        {
            texture2D = level.loader.GetTexture(textureName);
            playerTexture2D = level.loader.GetTexture(playerTextureName);
        }
        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            UpdAnimation(elapsedTime);
            spriteBatch.Draw(texture2D, position, texture2D.Bounds, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0f);
            spriteBatch.Draw(playerTexture2D, position + offsetVector, texture2D.Bounds, Color.White, 0, Vector2.Zero, pminiscale, SpriteEffects.None, 0f);
        }

        float NextLevel(int num)
        {
            float res =  712 + 512 / scale * 3 * num - 200;
            if (num == 9) res += 300;
            return res;
        }

        private void UpdAnimation(double elapsedTime)
        {
            if (cd > 0)
            {
                cd -= (float)elapsedTime;
            } else
            {
                offsetVector.Y += (float)elapsedTime * speed;
                if (offsetVector.Y > targetPos) { offsetVector.Y = targetPos; stoped = true; }
            }
        }
    }
}
