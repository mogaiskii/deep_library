﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public class GameMap
    {
        Tile[,] tiles;
        Level level;
        public int Width
        {
            get { return width; }
        }
        int width;
        public int Height
        {
            get { return height; }
        }
        int height;
        public static int TILE_SIZE = 512;

        public GameMap(Level level)
        {
            this.level = level;
            level.SetMap(this);
        }
        private TileCollision GetMapCollision(int x, int y)
        {
            if (x >= width || y >= height)
            {
                return TileCollision.blocking;
            }

            Tile tile = tiles[y, x];
            if (!(tile is null))
            {
                return tile.Collision;
            }

            return TileCollision.passing;
        }

        public TileCollision GetCollision(int x, int y)
        {
            if (x < 0 || y < 0) return TileCollision.blocking;
            x = x / TILE_SIZE;
            y = y / TILE_SIZE;
            return GetMapCollision(x, y);
        }

        public Tile[] LoadTiles(string[,] tileCodes)
        {
            height = tileCodes.GetUpperBound(0) + 1;
            width = tileCodes.GetUpperBound(1) + 1;
            tiles = new Tile[height, width];
            Dictionary<string, Tile> tileMap = new Dictionary<string, Tile>();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    string code = tileCodes[y, x];
                    //if (code == TileGenerator.empty) continue;
                    Tile tile;
                    if (tileMap.ContainsKey(code))
                    {
                        tile = tileMap[code];
                        tile.AddCoords(new Vector2(TILE_SIZE * x, TILE_SIZE * y));
                    } else
                    {
                        tile = TileGenerator.ByCode(code, level);
                        if (tile is null) continue;
                        tile.SetMultipleCoords(new Vector2(TILE_SIZE * x, TILE_SIZE * y));
                        tileMap.Add(code, tile);
                    }
                    tiles[y, x] = tile;
                }
            }
            Tile[] res_tiles = new Tile[tileMap.Count];
            tileMap.Values.CopyTo(res_tiles, 0);
            return res_tiles;
        }

        internal HashSet<Point> GetAdjacentPoints(int x, int y)
        {
            HashSet<Point> adj = new HashSet<Point>();
            for (int i = -1; i <= 1; i++)
            {
                int cx = x + i;
                if (cx < 0 || cx > width) continue;

                for (int j = -1; j <= 1; j++)
                {
                    int cy = y + j;
                    if (cy < 0 || cy > height) continue;

                    if (GetMapCollision(cx, cy) == TileCollision.blocking) continue;

                    adj.Add(new Point(cx, cy));
                }
            }

            return adj;
        }
    }
}
