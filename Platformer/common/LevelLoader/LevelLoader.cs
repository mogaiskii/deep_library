﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common.LevelLoader
{
    public interface LevelLoader
    {
        public Level LoadLevel(string name, Game1 game, ContentManager content);
    }
}
