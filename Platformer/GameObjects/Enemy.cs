﻿using Microsoft.Xna.Framework;
using Platformer.common;
using Platformer.Engines;
using Platformer.Weapons.Shots;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.GameObjects
{
    class Enemy : common.GameObject, common.Drawable, common.Alive
    {
        protected EnemyEngine engine;
        public override GameObjectGroups? Group { get => GameObjectGroups.enemy; protected set => base.Group = value; }


        public Enemy(Vector2 coordinates, common.Level level) : base(coordinates, level) {
            engine = new EnemyEngine(level, this);
        }

        public new void Update(double elapsedTime)
        {
            engine.Update(elapsedTime);

            if (currentDamageCoolDown > 0) currentDamageCoolDown -= (int)elapsedTime;

            IEnumerable<GameObject> shots = level.GetOverlaps(GetSelfZone(), GameObjectGroups.playerShot);
            if (!(shots is null))
            {
                foreach (GameObject shot in shots)
                {
                    ((WeaponShot)shot).PerformDamage(this);
                }
            }
        }
    }
}
