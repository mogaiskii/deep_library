﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer.common
{

    public class ChangeLevelExc : Exception
    {
        public ChangeLevelExc()
        {
        }

        public ChangeLevelExc(string message)
            : base(message)
        {
        }

        public ChangeLevelExc(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class Level
    {
        GameObjectRegistry objectRegistry;
        public UserInterface userInterface { get; protected set; }
        public GameMap map { get; private set; }

        public string nextLevel = "main";

        float levelCD = 300;

        public UIWidgets.ExitButton ExitButton;

        internal void Exit()
        {
            game.Exit();
        }

        public Collider collider;
        private Game1 game;
        bool paused = false;

        internal void Pause()
        {
            paused = !paused;
            if (paused) userInterface.AddWidget(ExitButton);
            else userInterface.RemoveWidget(ExitButton);
        }

        public GameObject player { get; private set; }

        public ResourceLoader loader { get; private set; }

        bool loopLock = false;
        protected List<GameObject> objectsToAdd = new List<GameObject>();
        protected List<GameObject> objectsToRemove = new List<GameObject>();


        public Level(ContentManager content, Game1 game)
        {
            objectRegistry = new GameObjectRegistry();
            loader = new ResourceLoader(content);
            this.game = game;
            userInterface =  new UserInterface(this);
            ExitButton = new UIWidgets.ExitButton(new Vector2(2500, 1500), this, game.controls);
        }

        public Level(ContentManager content, Game1 game, params GameObject[] objects)
        {
            objectRegistry = new GameObjectRegistry();
            AddObjects(objects);
            loader = new ResourceLoader(content);
            this.game = game;
            userInterface =  new UserInterface(this);
        }
        public void SetMap(GameMap map)
        {
            this.map = map;
            collider = new Collider(this, map);
        }

        public void SetPlayer(GameObjects.Player player)
        {
            this.player = player;
        }

        public void AddObjects(params GameObject[] objects)
        {
            if (loopLock)
            {
                foreach (var obj in objects)
                {
                    //objectRegistry.Add(obj);
                    objectsToAdd.Add(obj);
                }
            } else
            {
                foreach (var obj in objects)
                {
                    objectRegistry.Add(obj);
                    //objectsToAdd.Add(obj);
                }
            }
        }

        public void RemoveObject(GameObject item)
        {
            if (loopLock)
            {
                //objectRegistry.Remove(item);
                objectsToRemove.Add(item);
            }
            else
            {
                objectRegistry.Remove(item);
                //objectsToAdd.Add(item);
            }
        }

        internal void GameOver()
        {
            game.GameOver();
        }

        public void Load()
        {
            foreach (GameObject item in objectRegistry)
            {
                item.Load();
            }
            userInterface.Load();
            ExitButton.Load();
            levelCD = 300;
        }

        public UIWidgets.LevelAnimation animation = null;

        string goNext;
        bool shouldGoNext = false;
        void GoNextLevel()
        {
            game.NextLevel(goNext);
            throw new ChangeLevelExc();
        }
        public void NextLevel(string name)
        {
            goNext = name;
            shouldGoNext = true;
            paused = true;
        }
        public void Update(GameTime gameTime)
        {
            loopLock = true;

            double elapsedTime = gameTime.ElapsedGameTime.TotalMilliseconds;

            userInterface.Update(elapsedTime);
            if (levelCD > 0) levelCD -= (float)elapsedTime;

            if (!paused && levelCD <= 0)
            {
                foreach (Alive item in objectRegistry.GetAliveEnumerator())
                {
                    item.Update(elapsedTime);
                }
            }
            if (shouldGoNext)
            {
                if (!(animation is null) && !animation.stoped)
                {
                }
                else
                {
                    GoNextLevel();
                }
            }

            foreach (GameObject item in objectsToAdd)
            {
                objectRegistry.Add(item);
                item.Load();
            }
            objectsToAdd.Clear();

            foreach (GameObject item in objectsToRemove)
            {
                objectRegistry.Remove(item);
                //item.unLoad();
            }
            objectsToRemove.Clear();

            loopLock = false;
        }


        public void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (shouldGoNext)
            {
                if (!(animation is null))
                {
                    spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Matrix.CreateScale(Game1.SCALE));
                    animation.Draw(spriteBatch, elapsedTime);
                    spriteBatch.End();
                }
            } else
            {
                spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Matrix.CreateScale(Game1.SCALE));
                foreach (Drawable item in objectRegistry.GetDrawableEnumerator())
                {
                    item.Draw(spriteBatch, elapsedTime);
                }
                spriteBatch.End();

                spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Matrix.CreateScale(Game1.SCALE));
                userInterface.Draw(spriteBatch, elapsedTime);
                spriteBatch.End();
            }

        }

        public LinkedList<GameObject> GetOverlaps(Rectangle zone, GameObjectGroups group)
        {
            IEnumerable<GameObject> list = objectRegistry.GetEnumerable(group);
            if (list is null) return null;

            LinkedList<GameObject> resultList = new LinkedList<GameObject>();
            foreach (GameObject item in list)
            {
                if (item.HasOverlap(zone)) resultList.AddLast(item);
            }

            return resultList;
        }
    }
}
