﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;

namespace Platformer.UIWidgets
{
    public class Button : common.UIWidget, Alive
    {
        system.Controls ctrls;
        protected Texture2D texture2D;
        protected string textureName = "pause";

        protected string Text = null;

        protected Vector2 textPosition = new Vector2(100, 150);

        int debounce = 200;
        int debounceTimer = 0;
        public Button(Vector2 position, common.Level level, system.Controls controls) : base(position, level)
        {
            ctrls = controls;
        }

        public override void Load()
        {
            texture2D = level.loader.GetTexture(textureName);
        }

        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (!(texture2D is null)) 
                spriteBatch.Draw(texture2D, position, Color.White);

            if (!(Text is null))
                spriteBatch.DrawString(level.userInterface.font, Text, position + textPosition, Color.White);
        }

        protected virtual void OnClick() {
            debounceTimer = debounce;
        }

        public void Update(double elapsedTime)
        {
            Vector2 mouse = ctrls.MousePosition;
            Rectangle selfRect = new Rectangle(position.ToPoint(), new Point(GameMap.TILE_SIZE, GameMap.TILE_SIZE));
            Rectangle mouseRect = new Rectangle((int)mouse.X - 10, (int)mouse.Y - 10, 20, 20);
            if (selfRect.Intersects(mouseRect) && debounceTimer == 0 && ctrls.LeftClick) OnClick();
            if (debounceTimer > 0)
            {
                debounceTimer -= (int)elapsedTime;
                if (debounceTimer < 0) debounceTimer = 0;
            }
        }
    }
}
