﻿using Microsoft.Xna.Framework;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Weapons.Shots
{
    class Fire: WeaponShot
    {
        int ttl = 1000;
        protected override int TimeToLive { get => ttl; set => ttl = value; }
        private int additionalTtl = 120;
        protected override int AdditionalTTL { get => additionalTtl; set => additionalTtl = value; }
        private Rectangle bounds = new Rectangle(50, 50, 480, 480);
        public override Rectangle Bounds { get => bounds; protected set => bounds = value; }
        public Fire(Vector2 coordinates, Level level, int damage, Vector2 target, GameObject invoker) : base(coordinates, level, damage, target, invoker)
        {
            if (invoker.Group.HasValue && invoker.Group.Value == GameObjectGroups.player) textureName = "fire";
            else textureName = "ice";
            speed = 6f;
        }
        public override void Update(double elapsedTime)
        {
            TimeToLive -= (int)elapsedTime;
            if (TimeToLive < 0)
            {
                if (deactivated == false)
                {
                    deactivated = true;
                    speed /= 2f;
                    TimeToLive += AdditionalTTL;
                }
                else
                {
                    level.RemoveObject(this);
                }
            }

            Vector2 cMovement = movement * (float)speed * (float)elapsedTime;

            Vector2 nextCoords = coordinates + cMovement;
            if (level.collider.HasCollision(new Rectangle((int)nextCoords.X + Bounds.X, (int)nextCoords.Y + Bounds.Y, Bounds.Width, Bounds.Height))) {
                TimeToLive = 0;
            }

            MoveTo(nextCoords);
        }

    }
}
