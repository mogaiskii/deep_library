﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Platformer.common
{
    public static class TileGenerator
    {
        public const string empty = "e";
        static Dictionary<string, Tuple<string,TileCollision>> tileTextures = new Dictionary<string, Tuple<string, TileCollision>>
        {
            { "b", new Tuple<string,TileCollision>("platform", TileCollision.blocking) },
            { "bw", new Tuple<string,TileCollision>("wall_left", TileCollision.blocking) },
            { "be", new Tuple<string,TileCollision>("wall_right", TileCollision.blocking) },
            { "bs", new Tuple<string,TileCollision>("wall_bottom", TileCollision.blocking) },
            { "bp", new Tuple<string,TileCollision>("wood", TileCollision.blocking) }
        };
        public static Tile ByCode(string code, Level level)
        {
            Tuple<string, TileCollision> tileData;
            if (tileTextures.TryGetValue(code, out tileData))
            {
                return new Tile(tileData.Item1, Vector2.Zero, level, tileData.Item2);
            } else
            {
                return new Tile("floor tile 4.13", Vector2.Zero, level, TileCollision.passing);
            }
        }
    }
}
