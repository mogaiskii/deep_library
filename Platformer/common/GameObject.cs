﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Platformer.common
{
    //Не реализует Alive и Drawable , чтобы можно было определять классы, не имеющие Update или Draw
    public class GameObject
    {
        public static int startHealth = 1;
        public virtual int StartHealth { get => startHealth; set => GameObject.startHealth = value; }

        public int currentHealth { get; protected set; } = 1;

        protected string textureName;
        protected Texture2D texture2D;
        public Vector2 coordinates { get; protected set; }
        protected Color _textureBgColor = Color.White;
        protected Level level;

        public static int damageCoolDown = 300; //ms
        public int currentDamageCoolDown = 0; //ms

        private Rectangle defaultBounds = new Rectangle(0, 0, 512, 512);
        public virtual Rectangle Bounds { 
            get { return defaultBounds; } 
            protected set { defaultBounds = value; } 
        }

        private readonly GameObjectGroups? defaultGroup = null;
        public virtual GameObjectGroups? Group { get { return defaultGroup; } protected set { } }

        public static Weapons.Weapon defaultWeapon = new Weapons.Sword();
        public virtual Weapons.Weapon UsedWeapon { get => defaultWeapon; protected set => defaultWeapon = value; }

        public GameObject(Vector2 coordinates, Level level)
        {
            this.coordinates = coordinates;
            this.level = level;
            currentHealth = StartHealth;
        }
        public GameObject(string textureName, Vector2 coordinates, Level level)
        {
            this.textureName = textureName;
            this.coordinates = coordinates;
            this.level = level;
            currentHealth = StartHealth;
        }

        public GameObject(string textureName, Vector2 coordinates, Level level, Color textureBgColor)
        {
            this.textureName = textureName;
            this.coordinates = coordinates;
            this._textureBgColor = textureBgColor;
            this.level = level;
            currentHealth = StartHealth;
        }

        public void HandleDamage(int damage)
        {
            if (currentDamageCoolDown > 0) return;

            currentHealth -= damage;
            if (currentHealth < 0) Death();

            currentDamageCoolDown = damageCoolDown;
        }

        protected void Death()
        {
            if (Group.HasValue && Group.Value == GameObjectGroups.player) level.GameOver();
            else level.RemoveObject(this);
        }

        // TODO: выгрузка из памяти
        public void Load()
        {
            //texture2D = content.Load<Texture2D>(textureName);
            texture2D = level.loader.GetTexture(textureName);
        }

        public virtual void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;
            spriteBatch.Draw(texture2D, coordinates, _textureBgColor);
        }

        public virtual void Update(double elapsedTime)
        {
            if (currentDamageCoolDown > 0) currentDamageCoolDown -= (int)elapsedTime;
        }

        public virtual void OnMovement(Vector2 diffMovement)
        {

        }

        public void Move(Vector2 movement)
        {
            coordinates += movement;
            OnMovement(movement);
        }
        public void MoveTo(Vector2 target)
        {
            Vector2 movement = target - coordinates;
            Move(movement);
        }

        public Rectangle GetSelfZone()
        {
            return new Rectangle((int)coordinates.X + Bounds.X, (int)coordinates.Y + Bounds.Y, Bounds.Width, Bounds.Height);
        }

        internal bool HasOverlap(Rectangle zone)
        {
            Rectangle selfZone = GetSelfZone();

            return zone.Intersects(selfZone);
        }
    }
}
