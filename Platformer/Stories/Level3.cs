﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level3 : TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,b,b,b,b,b,b,b,bp
bw,e,e,e,e,e,e,e,bp,e,e,be
bw,e,bp,e,e,e,e,e,bp,e,e,be
bw,e,bp,e,y,e,e,e,bp,e,e,be
bw,g,bp,e,e,e,e,e,bp,p,e,be
bw,bp,bp,e,e,e,e,e,b,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bp,bs,bs,bs,bs,bs,bs,bs,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 3;
        }
        public override string NextLevel()
        {
            return "level4";
        }
    }
}
