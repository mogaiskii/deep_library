﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.UIWidgets
{
    class WeaponBar : common.UIWidget
    {
        public static int offset = common.GameMap.TILE_SIZE;
        GameObjects.Player player;
        protected Texture2D texture2D;
        static string textureName = "icon";
        protected Texture2D cooldownTexture2D;
        static string cooldownTextureName = "icon cool down";

        public WeaponBar(Vector2 position, common.Level level, GameObjects.Player player) : base(position, level)
        {
            this.player = player;
        }

        public override void Load()
        {
            texture2D = level.loader.GetTexture(textureName);
            cooldownTexture2D = level.loader.GetTexture(cooldownTextureName);
        }


        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;

            spriteBatch.Draw(texture2D, position, Color.White);
            spriteBatch.Draw(texture2D, position + new Vector2(offset, 0), Color.White);

            Weapons.Weapon left = player.LeftWeapon();
            Weapons.Weapon right = player.RightWeapon();

            Texture2D leftTexture = level.loader.GetTexture(left.IconName);
            Texture2D rightTexture = level.loader.GetTexture(right.IconName);

            spriteBatch.Draw(leftTexture, position, Color.White);
            spriteBatch.Draw(rightTexture, position + new Vector2(offset, 0), Color.White);

            int leftCD = left.CooldownPercent();
            if (leftCD > 0)
            {
                spriteBatch.Draw(
                    cooldownTexture2D, 
                    position, 
                    new Rectangle(0, 0, common.GameMap.TILE_SIZE, (int)((leftCD / 100f) * common.GameMap.TILE_SIZE)), 
                    Color.White
                );
            }

            int rightCD = right.CooldownPercent();
            if (rightCD > 0)
            {
                spriteBatch.Draw(
                    cooldownTexture2D, 
                    position + new Vector2(offset, 0),
                    new Rectangle(0, 0, common.GameMap.TILE_SIZE, (int)((rightCD / 100f) * common.GameMap.TILE_SIZE)),
                    Color.White
                );
            }
        }
    }
}
