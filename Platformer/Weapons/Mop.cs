﻿using Microsoft.Xna.Framework;
using Platformer.common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Weapons
{
    class Mop : Weapon
    {
        public Mop()
        {
            damage = 8;
            singleWeaponDamage = 14;
        }

        public override GameObject Perform(bool singleWeapon, GameObject invoker, Vector2 invokePoint, Vector2 targetPoint, Level level)
        {
            if (currentCooldown > 0) return null;
            currentCooldown = cooldown;
            int realDamage;
            if (singleWeapon) realDamage = singleWeaponDamage;
            else realDamage = damage;
            return new Shots.Melee(invokePoint, level, realDamage, targetPoint, invoker);
        }
    }
}
