﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level6: TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,bp,b,b,b,b,b,b,b,b,bp
bw,e,bp,e,e,e,e,e,e,e,e,be
bw,e,b,e,e,e,e,e,y,g,e,be
bw,e,e,e,e,e,e,bp,e,e,e,be
bw,e,e,e,bp,y,e,bp,e,e,e,be
bw,k,b,e,b,e,e,b,e,e,e,be
bw,e,e,b,e,e,e,e,k,e,e,be
bw,e,e,p,e,e,e,bp,e,e,e,be
bp,bs,bs,bs,bs,bs,bs,bp,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 6;
        }
        public override string NextLevel()
        {
            return "level7";
        }
    }
}
