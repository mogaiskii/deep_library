﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Platformer.common;
using Platformer.GameObjects;

namespace Platformer.Stories
{
    public class Start : TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,b,b,b,b,b,b,b,bp
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,g,e,e,e,e,p,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bw,e,e,e,e,e,e,e,e,e,e,be
bp,bs,bs,bs,bs,bs,bs,bs,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 1;
        }

        public override string NextLevel()
        {
            return "level2";
        }

    }
}
