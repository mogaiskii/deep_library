﻿using Microsoft.Xna.Framework;


namespace Platformer.common
{
    public interface Alive
    {
        public void Update(double elapsedTime);
    }
}
