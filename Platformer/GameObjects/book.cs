﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer.common;
using Platformer.Engines;
using Platformer.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.GameObjects
{
    class Book: Enemy
    {
        public static Rectangle bounds = new Rectangle(150, 60, 230, 420);
        public override Rectangle Bounds { get => Book.bounds; protected set => Book.bounds = value; }

        public static int BookHealth = 70;
        public override int StartHealth { get => BookHealth; set => BookHealth = value; }

        public static Weapon BookWeapon = new AttackBook();
        public override Weapon UsedWeapon { get => BookWeapon; protected set => BookWeapon = value; }

        public bool horMove = true;
        public bool laying = true;
        int frame = 0;
        int maxFrame = 1;
        public static int frameTimer = 150;
        int cFrameTimer = 150;

        //описывать данные для загрузки класса в сам класс. в т ч текстуру и прочее. а вот загружать текстуру лучше уровнем выше, чтобы загрузить её лишь раз!
        public Book(Vector2 coordinates, common.Level level) : base(coordinates, level)
        {
            bounds = new Rectangle(150, 60, 230, 420);
            textureName = "book";
            engine = new BookEngine(level, this);
        }

        Rectangle SourceRect()
        {
            int line = 0;
            if (horMove)
            {
                line = 1;
            }
            if (!laying)
            {
                if (cFrameTimer < 0)
                {
                    cFrameTimer = frameTimer;
                    frame += 1;
                    if (frame > maxFrame) frame = 0;
                }
            }
            return new Rectangle(frame * GameMap.TILE_SIZE, line * GameMap.TILE_SIZE, GameMap.TILE_SIZE, GameMap.TILE_SIZE);
        }

        public override void OnMovement(Vector2 diffMovement)
        {
            base.OnMovement(diffMovement);
            laying = false;
        }

        public override void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {

            if (texture2D is null) return;
            spriteBatch.Draw(texture2D, coordinates, SourceRect(), _textureBgColor);
            cFrameTimer -= (int)elapsedTime;
        }

    }
}
