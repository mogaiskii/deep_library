﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.Stories
{
    class Level4:TypicalStory
    {
        public override string GetMap()
        {
            string mapStr = @"bp,b,b,b,b,b,b,bp,b,b,b,bp
bw,e,e,e,e,e,e,bp,e,e,e,be
bw,e,e,e,e,e,e,bp,e,k,e,be
bw,e,e,e,e,y,e,b,e,b,e,be
bw,e,k,b,bp,e,e,e,p,e,e,be
bw,e,e,e,be,e,e,e,e,e,e,be
bw,e,g,e,be,e,e,e,e,e,e,be
bw,e,e,e,be,e,e,e,e,e,e,be
bp,bs,bs,bs,bp,bs,bs,bs,bs,bs,bs,bp";
            return mapStr;
        }
        protected override int GetNum()
        {
            return 4;
        }
        public override string NextLevel()
        {
            return "level5";
        }
    }
}
