﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Platformer.common
{
    public enum TileCollision
    {
        passing,
        blocking
    }
    public class Tile : GameObject, Drawable
    {
        List<Vector2> multipleCoords = null;
        public TileCollision Collision
        {
            get { return collision; }
        }
        TileCollision collision;
        public Tile(String textureName, Vector2 coordinates, Level level, TileCollision collision) : base(textureName, coordinates, level)
        {
            this.collision = collision;
        }
        public Tile(String textureName, Vector2 coordinates, Level level, Color textureBgColor, TileCollision collision) : base(textureName, coordinates, level, textureBgColor)
        {
            this.collision = collision;
        }
        public void SetMultipleCoords(params Vector2[] coords)
        {
            multipleCoords = new List<Vector2>(coords);
        }
        public void AddCoords(Vector2 coords)
        {
            if (multipleCoords is null) multipleCoords = new List<Vector2>();
            multipleCoords.Add(coords);
        }

        public new void Draw(SpriteBatch spriteBatch, double elapsedTime)
        {
            if (texture2D is null) return;

            if (multipleCoords is null)
            {
                spriteBatch.Draw(texture2D, coordinates, _textureBgColor);
            }
            else
            {
                foreach (var item in multipleCoords)
                {
                    spriteBatch.Draw(texture2D, item, _textureBgColor);
                }
            }
        }
    }
}
